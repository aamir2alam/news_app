class User {
  String _uid;
  String _name;

  User(this._uid, this._name);

  set uid(String uid) {
    this._uid = uid;
  }

  set name(String name) {
    this._name = name;
  }

  String get uid => this._uid;

  String get name => this._name;
}
