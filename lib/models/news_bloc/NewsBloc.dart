//TODO: imports

// TODO: List of employees

// TODO:  Stream controllers

//TODO:  Stream Sink getters

//TODO:  Constructor - add data; Listen to changes

// TODO:  Core functions

// TODO:  dispose

import 'dart:async';
import 'News.dart';

class NewsBloc {
  // sink to add in pipe
  // stream to get data from pipe
  // pipe: Data flow

  final _newsStreamController = StreamController<News>();

  // for increment and dec

  final _currentNewsUpdateController = StreamController<News>();
  // final _employeeSalaryDecrementStreamController = StreamController<Employee>();

  // getters for data

  Stream<News> get newsStream => _newsStreamController.stream;
  StreamSink<News> get newsSink => _newsStreamController.sink;

  // getters for actions
  // StreamSink<News> get currentNewsUpdate =>
  //     _currentNewsUpdateController.sink;

  // contrcutor
  NewsBloc() {
    // add data to the stream controller
    // _newsStreamController.add();

    // _currentNewsUpdateController.stream.listen(_updateCurrentNews);
  }

  // _incrementSalary(Employee employee) {
  //   double salary = employee.salary;
  //   double increment = salary * 20 / 100;
  //   _employeeList[employee.id - 1].salary = salary + increment;

  //   employeeListSink.add(_employeeList);
  // }

  // _decrementSalary(Employee employee) {
  //   double salary = employee.salary;
  //   double increment = salary * 20 / 100;
  //   _employeeList[employee.id - 1].salary = salary - increment;

  void dispose() {
    // _employeeSalaryDecrementStreamController.close();
    // _employeeSalaryIncrementStreamController.close();
    _newsStreamController.close();
  }
}
