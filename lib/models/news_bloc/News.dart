class News {
  String _id;
  String _url;
  String _title;
  String _desc;
  String _img;
  String _web_url;
  int _likes;
  bool _isLiked;

  News(this._id, this._url, this._web_url, this._title, this._desc, this._likes, this._isLiked,
      this._img);

  // setters

  set id(String id) {
    this._id = url;
  }

  set url(String url) {
    this._url = url;
  }
  set web_url(String url) {
    this._web_url = url;
  }

  set title(String title) {
    this._title = title;
  }

  set desc(String desc) {
    this._desc = desc;
  }

  set likes(int likes) {
    this._likes = likes;
  }

  set isLiked(bool isLiked) {
    this._isLiked = isLiked;
  }

  set img(String img) {
    this._img = img;
  }

  // getters

  String get id => this._id;
  String get url => this._url;
  String get web_url => this._web_url;
  String get title => this._title;
  String get desc => this._desc;
  int get likes => this._likes;
  bool get isLiked => this._isLiked;
  String get img => this._img;
}
