import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:news_app/models/user/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;

  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(user.uid, "") : null;
  }

  Stream<User> get user {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  //signin anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;

      return user;
    } catch (e) {}
  }

  createNewFirebaseUser(var user) {
    _db.collection("guest_users").add(user);
  }

  Future signInWithGoogle(var credential) async {
    try {
      AuthResult result = await _auth.signInWithCredential(credential);
      FirebaseUser user = result.user;
      return user;
    } catch (e) {}
  }

  Future signOut() async {
    try {
      await _auth.signOut();
    } catch (e) {}
  }
}
