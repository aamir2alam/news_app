import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:news_app/resources/strings.dart';

class NewsDbProvider with ChangeNotifier {
  String base_url = 'https://news-app-backend.ewylbox.lastcampus.com/';
  final storage = new FlutterSecureStorage();
  static String default_language = "english";

  void setDefaultLanguage(String lang) {
    if (lang == AppStrings.englishString) {
      default_language = "english";
    } else {
      default_language = "hindi";
    }
  }

  Future getNews(int page, int lang) async {
    String storedToken = await storage.read(key: "token");

    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-auth-token": storedToken
    };
    print('language recieved $lang');

    if(lang == 0) {
      default_language = "english";
    }else{
      default_language = "hindi";
    }
    String url = base_url +
        "api/news/getLatestNews/${page.toString()}?language=${default_language}";
    print(url);
    var response = await http.get(url, headers: headers);

//    print(response.body.length == 2);
//    print(response.statusCode);

    return json.decode(response.body);
  }

  Future getLikedNews(int page) async {
    String storedToken = await storage.read(key: "token");

    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-auth-token": storedToken
    };

    String url = base_url + "api/news/getLikedNews/${page.toString()}";
    var response = await http.get(url, headers: headers);

    print(response.body);

    return json.decode(response.body);
  }

  Future<bool> likeNewsbyId(bool isLiked, String newsId) async {
    print("liked");
    // setState(() {
    //   if (!slideList[currentPage]["is_liked"]) {
    //     slideList[currentPage]["likes_count"] += 1;
    //     slideList[currentPage]["is_liked"] = false;
    //   } else {
    //     slideList[currentPage]["likes_count"] -= 1;
    //     slideList[currentPage]["is_liked"] = true;
    //   }
    // });

    String storedToken = await storage.read(key: "token");
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-auth-token": storedToken
    };
    String url = "${base_url}api/news/likeNews/${newsId}";
    var response = http.post(url, headers: headers);

    return !isLiked;
  }
}
