import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthServiceProvider with ChangeNotifier {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;
  String _uid;
  String base_url = 'https://news-app-backend.ewylbox.lastcampus.com';

  AuthServiceProvider(this._uid);
  final storage = new FlutterSecureStorage();

  getUid() => this._uid;
  setUid(String uid) => _uid = uid;

  Future signInAnon() async {

    print("called signInAnon");
    String storedToken = await storage.read(key: "token");
    if (storedToken != null) {
      print("checking stored token");
      Map<String, dynamic> userObject = parseJwt(storedToken);
      print(userObject);
      int currentTime =
      (new DateTime.now().microsecondsSinceEpoch / (1000 * 1000)).floor();
      if (userObject['exp'] < currentTime) {
        print("Token Expired");
        await removeToken();
      } else {
        _uid = userObject["user"]["id"];
        print("Using Stored Token");
//        notifyListeners();
        return _uid;

      }
    }

    print("making new request");
    AuthResult result = await _auth.signInAnonymously();
    FirebaseUser user = result.user;
    print("firebase called");
    Map<String, String> headers = {"Content-type": "application/json"};
    String body = '{"firebase_id": "${user.uid}"}';
    String url =
        'https://news-app-backend.ewylbox.swipeupnews.com/api/auth/anonLogin';

    var response = await http.post(url, headers: headers, body: body);
    print(response.statusCode);
    print(response.body);
    String tokenString = json.decode(response.body)["token"];
    await storeToken(tokenString);
    Map<String, dynamic> userObject = parseJwt(tokenString);
    print(userObject);

    _uid = userObject["user"]["id"];
    return _uid;
//    notifyListeners();
  }

  Future storeToken(String token) async {
    await storage.write(key: "token", value: token);
  }

  Future removeToken() async {
    await storage.delete(key: "token");
  }

  Map<String, dynamic> parseJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }

    return payloadMap;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return utf8.decode(base64Url.decode(output));
  }

  // User _userFromFirebaseUser(FirebaseUser user) {
  //   return user != null ? User(user.uid, "") : null;
  // }

  // Stream<User> get user {
  //   return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  // }

  // //signin anon
  // Future signInAnon() async {
  //   try {
  //     AuthResult result = await _auth.signInAnonymously();
  //     FirebaseUser user = result.user;

  //     return user;
  //   } catch (e) {}
  // }

  // createNewFirebaseUser(var user) {
  //   _db.collection("guest_users").add(user);
  // }

  // Future signInWithGoogle(var credential) async {
  //   try {
  //     AuthResult result = await _auth.signInWithCredential(credential);
  //     FirebaseUser user = result.user;
  //     return user;
  //   } catch (e) {}
  // }

  // Future signOut() async {
  //   try {
  //     await _auth.signOut();
  //   } catch (e) {}
  // }
}
