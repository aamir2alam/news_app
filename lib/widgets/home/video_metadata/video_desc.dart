import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';

Widget videoDesc(News news) {
  return Container(
    padding: EdgeInsets.only(left: 16),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Container(
          child: Text('Headline: ' + news.title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w400)),
        ),
      ],
    ),
  );
}
