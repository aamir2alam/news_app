import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/pages/news_detail/news_web.dart';
import 'package:page_transition/page_transition.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter_widgets/flutter_widgets.dart';

class AppVideoPlayer extends StatefulWidget {
  final News news;
  final bool isPause;
  AppVideoPlayer({Key key, @required this.news, @required this.isPause})
      : super(key: key);
  @override
  _AppVideoPlayerState createState() => _AppVideoPlayerState();
}

class _AppVideoPlayerState extends State<AppVideoPlayer> {
  VideoPlayerController _controller;
  bool _is_video_started = false;

  @override
  void initState() {
    setState(() {
      _is_video_started = false;
    });
    super.initState();
    _controller = VideoPlayerController.network(widget.news.url)
      ..initialize().then((_) {
        //_controller.play();
        setState(() {
          _is_video_started = true;
        });
        print("video started");
        setState(() {});
      });

  }

  void tapAction() {
    setState(() {
      // If the video is playing, pause it.
      if (_controller.value.isPlaying) {
        _controller.pause();
      } else {
        // If the video is paused, play it.
        _controller.play();
      }
    });
  }

  void doubleTapAction() {}
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    var width = queryData.size.width * 1.30;
    var height = queryData.size.height * 1.2;
    if (widget.isPause) {
      if (_controller != null) _controller.pause();
    }
    return Center(
        child: _controller.value.initialized
            ? AspectRatio(
                aspectRatio: _controller.value.aspectRatio < 1
                    ? width / height
                    : _controller.value.aspectRatio,
                child: GestureDetector(
                    child: VisibilityDetector(
                      key: Key(widget.news.id),
                      onVisibilityChanged: (VisibilityInfo info) {
                        //debugPrint("${info.visibleFraction} of my widget is visible");
                        if (info.visibleFraction == 1) {
                          if (_controller != null) _controller.play();
                        } else {
                          if (_controller != null) _controller.pause();
                        }
                      },
                      child: VideoPlayer(_controller),
                    ),
                    onTap: tapAction,
                    onDoubleTap: doubleTapAction))
            : Stack(
                children: <Widget>[
                  Container(
                    child: widget.news.img != null ? Image.network(
                        widget.news.img) : Image.asset(
                        "assets/images/default_thumb.jpg"),
                  ),
                  Positioned(
                    child: SpinKitDoubleBounce(
                        color: Colors.redAccent, ),
                  )
                ],
              ));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.pause();
    _controller.dispose();
    _controller = null;
  }
}
