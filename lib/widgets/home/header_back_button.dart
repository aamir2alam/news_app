import 'package:flutter/material.dart';
import 'package:news_app/resources/dimen.dart';
import 'package:news_app/resources/strings.dart';

Widget headerBackButton(context) {

  onBackButton() {
    Navigator.pop(context);
  }
  return Container(
    margin: EdgeInsets.only(top: 60, left: 20, right: 20, bottom: 20),
    height: Dimen.headerHeight,
    alignment: Alignment.center,
    child: Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          child: IconButton(
              enableFeedback: true,
              icon: Icon(Icons.keyboard_backspace, color: Colors.white),
              onPressed: onBackButton),
        )
      ],
    ),
  );
}
