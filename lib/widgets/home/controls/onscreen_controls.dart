import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/widgets/home/controls/video_control_action.dart';
import 'package:news_app/widgets/home/video_metadata/video_desc.dart';

Widget onScreenControls(
    News news, likehandler, sharehandler, downloading, progress) {
  return Container(
      child: Column(
    mainAxisAlignment: MainAxisAlignment.end,
    children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          downloading
              ? Container(
                  width: 100,
                  height: 100,
                  margin: EdgeInsets.only(bottom: 100),
                  child: Card(
                    color: Colors.black38,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(
                          backgroundColor: Colors.redAccent,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          progress,
                          style: TextStyle(color: Colors.redAccent),
                        )
                      ],
                    ),
                  ),
                )
              : Text("")
        ],
      ),
      Row(
        children: <Widget>[
//            Expanded(flex: 5, child: videoDesc(news)),
          Expanded(flex: 4, child: Text("")),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(bottom: 60, right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  // userProfile(news),
                  videoControlAction(
                      icon: Icons.favorite_border,
                      label: news.likes.toString(),
                      likeHandler: likehandler,
                      shareHandler: sharehandler,
                      news: news),
                  // videoControlAction(icon: Icons.chat_bubble, label: "130")
                  // SpinnerAnimation(body: audioSpinner())
                ],
              ),
            ),
          )
        ],
      )
    ],
  ));
}
