import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/resources/dimen.dart';
import 'package:news_app/services/newsDbProvider.dart';
import 'package:news_app/widgets/home/controls/inapp_browser.dart';
import 'package:provider/provider.dart';
import 'package:like_button/like_button.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

Widget videoControlAction(
    {IconData icon,
    String label,
    double size = 35,
    likeHandler,
    shareHandler,
    News news}) {
  // final db = Provider.of<NewsDbProvider>(context);
  final ChromeSafariBrowser browser =
      new MyChromeSafariBrowser(new MyInAppBrowser());
  return Padding(
    padding: EdgeInsets.only(top: 10, bottom: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          margin: new EdgeInsets.only(right: 13.0),
          child: IconButton(
            color: Colors.white,
            icon: Icon(
              Icons.assignment,
              size: 44,
            ),
            onPressed: () async {
              await browser.open(
                  url: news.web_url,
                  options: ChromeSafariBrowserClassOptions(
                      android:
                          AndroidChromeCustomTabsOptions(addShareButton: false),
                      ios: IOSSafariOptions(barCollapsingEnabled: true)));
            },
          ),
        ),
        SizedBox(height: 30),
        LikeButton(
          isLiked: news.isLiked,
          likeCount: news.likes,
          countPostion: CountPostion.bottom,
          size: 48,
          onTap: (bool isLiked) {
            return likeHandler(isLiked, news.id);
          },
          likeBuilder: (bool isLiked) {
            return Icon(
              Icons.favorite,
              color: isLiked ? Colors.redAccent : Colors.white,
              size: 48,
            );
          },
          countBuilder: (int count, bool isLiked, String text) {
            var color = isLiked ? Colors.redAccent : Colors.white;
            Widget result;
            result = Text(
              text,
              style: TextStyle(color: color),
            );
            return result;
          },
        ),
        SizedBox(height: 20),
        Container(
//          margin: EdgeInsets.fromLTRB(0.0, 30.0, 10.0, 0.0),
          width: 54,
          height: 54,
          child: IconButton(
            icon: Container(
              child: Image(
                image: AssetImage(
                  "assets/images/whatsappIcon.png",
                ),
                fit: BoxFit.cover,
              ),
              height: 264,
              width: 264,
            ),
            iconSize: 264.0,
            onPressed: shareHandler,
          ),
        )
      ],
    ),
  );
}
