import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:news_app/animations/spinner_animation.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: SpinKitDoubleBounce(
      color: Colors.redAccent,
    ),
      color: Colors.black87,
    );
  }
}
