import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/pages/single_video/singleVideo.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:news_app/widgets/home/header_back_button.dart';
import 'package:news_app/widgets/home/home_header.dart';

final SlidableController slidableController = SlidableController();

Widget makeCard(News news, context, {likeHandler, shareHandler}) => Container(
      width: double.infinity,
      margin: EdgeInsets.all(10.0),
      alignment: Alignment.center,
      height: 300,
      child: Card(
        margin:
            new EdgeInsets.only(left: 20.0, right: 20.0, top: 8.0, bottom: 5.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                topLeft: Radius.circular(10),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10))),
        elevation: 4.0,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      topLeft: Radius.circular(10)),
                ),
                child: Card(
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            topLeft: Radius.circular(10)),
                        image: DecorationImage(
                          image: NetworkImage(news.img != null
                              ? news.img
                              : "https://image.freepik.com/vrije-vector/blue-breaking-news-tv-background_1017-14201.jpg"),
                          fit: BoxFit.cover,
                          colorFilter: ColorFilter.mode(
                              Colors.black.withOpacity(0.8), BlendMode.dstATop),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(5.0),
                                child: Ink(
                                  decoration: const ShapeDecoration(
                                    shape: CircleBorder(),
                                    color: Colors.black26,
                                  ),
                                  child: IconButton(
                                      icon: Icon(Icons.delete),
                                      color: Colors.white,
                                      onPressed: () => likeHandler(news.id)),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5.0),
                                child: Ink(
                                  decoration: const ShapeDecoration(
                                    shape: CircleBorder(),
                                    color: Colors.black26,
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.share),
                                    color: Colors.grey,
                                    onPressed: () =>
                                        shareHandler(news.title, news.url),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(10.0),
                                child: Opacity(
                                  opacity: 0.8,
                                  child: Ink(
                                    decoration: const ShapeDecoration(
                                      shape: CircleBorder(),
                                    ),
                                    child: IconButton(
                                      icon: Icon(Icons.play_circle_filled),
                                      iconSize: 80,
                                      color: Colors.red,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Stack(
                                                      children: <Widget>[

                                                        SingleVideo(
                                                          news: news,
                                                        ),
                                                        headerBackButton(context)
                                                      ],
                                                    )
                                                    ));
                                      },
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      )),
                ),
              ),
              Container(
                  height: 80,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(40)),
                  ),
                  padding: EdgeInsets.all(10.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.only(topRight: Radius.circular(40)),
                    ),
                    child: RichText(
                      overflow: TextOverflow.clip,
                      strutStyle: StrutStyle(fontSize: 24.0),
                      text: TextSpan(
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                letterSpacing: .3,
                                fontSize: 20),
                          ),
                          text: news.title),
                    ),
                  )),
            ]),
      ),
    );

class _MoreButton extends StatelessWidget {
  const _MoreButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool open = false;
    return IconButton(
      icon: Icon(Icons.keyboard_arrow_right, color: Colors.red, size: 30.0),
      iconSize: 20.0,
      onPressed: () =>
          Slidable.of(context).open(actionType: SlideActionType.secondary),
    );
  }
}
