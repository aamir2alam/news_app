import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/pages/like_page/makeCard.dart';
import 'package:news_app/pages/like_page/topAppbar.dart';
import 'package:news_app/services/newsDbProvider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_share_me/flutter_share_me.dart';

class LikedPage extends StatefulWidget {
  @override
  _LikedPageState createState() => _LikedPageState();
}

class _LikedPageState extends State<LikedPage> {
  ScrollController _scrollController = new ScrollController();
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  bool isPerformingRequest = false;
  bool isPerformingInitialRequest = false;

  // keep track of current page to avoid unncessary renders
  int currentFetchPage = 1;
  List slideList = [];

  @override
  void initState() {
    // TODO: implement initState
    new Future.delayed(Duration.zero, () async {
      final db = Provider.of<NewsDbProvider>(context, listen: false);
      setState(() => isPerformingInitialRequest = true);
      List items = await db.getLikedNews(currentFetchPage);
      currentFetchPage = currentFetchPage + 1;
      print(items.length);
      setState(() {
        slideList.addAll(items);
        isPerformingInitialRequest = false;
      });
    });

    // Set state when page changes
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          currentFetchPage != -1) {
        new Future.delayed(Duration.zero, () async {
          setState(() => isPerformingRequest = true);
          final db = Provider.of<NewsDbProvider>(context, listen: false);
          List items = await db.getLikedNews(currentFetchPage);
          currentFetchPage = currentFetchPage + 1;
          if (items.length < 10) currentFetchPage = -1;
          print(items.length);
          print(currentFetchPage);
          setState(() {
            slideList.addAll(items);
            isPerformingRequest = false;
          });
        });
      }
    });
  }

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  void likeHandler(newsId) {
    new Future.delayed(Duration.zero, () async {
      final db = Provider.of<NewsDbProvider>(context, listen: false);
      db.likeNewsbyId(true, newsId);
      var index = slideList.indexWhere((item) => item['_id'] == newsId);
      print(index);
      var data = slideList.removeAt(index);
      String _id = data["_id"];
      String url = data["content"]["url"];
      String web_url = data["url"];
      String title = data["title"];
      String desc = data["description"];
      int likes = data["likes_count"];
      bool isLiked = data["is_liked"];
      String img = data["img"];
      News news = News(_id, url,web_url, title, desc, likes, isLiked, img);

      _listKey.currentState.removeItem(index,
          (BuildContext context, Animation<double> animation) {
        return FadeTransition(
          opacity: CurvedAnimation(parent: animation, curve: Interval(0, 1.0)),
          child: SizeTransition(
            sizeFactor:
                CurvedAnimation(parent: animation, curve: Interval(0.5, 1.0)),
            axisAlignment: 0.0,
            child: makeCard(news, context),
          ),
        );
      }, duration: Duration(milliseconds: 1000));
    });
  }

  Future<void> share(String title, String videoUrl) async {
    FlutterShareMe().shareToWhatsApp(
        msg: '$title'
            ' : '
            ' $videoUrl '
            ' Swipe daily short news videos on the go. '
            ' www.swipeupnews.com ');
  }

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<NewsDbProvider>(context);
    Widget child;

    if (isPerformingInitialRequest) {
      child = Stack(
        children: <Widget>[
          Scaffold(
            backgroundColor: Color.fromRGBO(0, 0, 0, 1.0),
            body: Align(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            ),
          ),
        ],
      );
    } else {
      child = AnimatedList(
        key: _listKey,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        initialItemCount: slideList.length,
        controller: _scrollController,
        itemBuilder:
            (BuildContext context, int currentIndex, Animation animation) {
          // to check which page is active

          if (currentIndex == slideList.length - 1) {
            _buildProgressIndicator();
          }

          // data to pass : slideList[currentIndex - 1]
          var data = slideList[currentIndex];
          String _id = data["_id"];
          String url = data["content"]["url"];
          String web_url = data["url"];
          String title = data["title"];
          String desc = data["description"];
          int likes = data["likes_count"];
          bool isLiked = data["is_liked"];
          String img = data["thumbnail"];
          News news = News(_id, url, web_url, title, desc, likes, isLiked, img);
          return FadeTransition(
            opacity: animation,
            child: makeCard(news, context,
                likeHandler: likeHandler, shareHandler: share),
          );
        },
      );
    }

    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
      appBar: topAppBar,
      body: Container(
        child: child,
      ),
    );
  }
}
