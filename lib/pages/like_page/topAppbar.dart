import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final topAppBar = AppBar(
  elevation: 0,
  backgroundColor: Color.fromRGBO(247, 247, 247, 1.0),
  centerTitle: false,
  title: Text(
    "Liked News",
    style: GoogleFonts.montserrat(
      textStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
          letterSpacing: .5,
          fontSize: 20),
    ),
  ),
);
