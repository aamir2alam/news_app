import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:news_app/pages/like_page/liked_news.dart';
import 'package:news_app/pages/loading.dart';
import 'package:news_app/pages/videos_slide.dart';
import 'package:news_app/resources/strings.dart';
import 'package:news_app/widgets/home/home_header.dart';
import 'package:provider/provider.dart';
import 'package:news_app/services/newsDbProvider.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:google_fonts/google_fonts.dart';

class BottomNavigationPage extends StatefulWidget {
  @override
  _BottomNavigationPageState createState() => _BottomNavigationPageState();
}

class _BottomNavigationPageState extends State<BottomNavigationPage> {
  int _selectedPage = 0;
  int selected_laguage = null;
  final storage = new FlutterSecureStorage();
  final PageController ctrl = PageController(initialPage: 0);

  ScrollPhysics physics;
  List<String> languageList = ["English", "हिन्दी"];
//  List<String> selectedLanguage = List();

  onLanguageChange() {
    _showReportDialog();
  }

  _showReportDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          //Here we will build the content of the dialog
          return AlertDialog(
            title: Text(
              "Language",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    letterSpacing: .5,
                    fontSize: 20),
              ),
            ),
            content: MultiSelectChip(
              languageList,
              languageList[selected_laguage],
              onSelectionChanged: (selectedList) {
                setState(() {
                  selected_laguage = selected_laguage == 0 ? 1 : 0;
                });
                setDefaultLanguage(selectedList[0]);
              },
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "Close",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        letterSpacing: .5,
                        fontSize: 16),
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  @override
  initState() {
    super.initState();

    getDefaultLanguage();
    physics = ScrollPhysics();
  }

  void getDefaultLanguage() async {
    new Future.delayed(Duration.zero, () async {
      final db = Provider.of<NewsDbProvider>(context, listen: false);

      String default_lang = await storage.read(key: "lang");

      print("default lang ${default_lang}");
      if (default_lang == null) {
        setState(() {
          selected_laguage = 1;
        });
        await storage.write(key: "lang", value: AppStrings.hindiString);
        return;
      }

      if (default_lang == languageList[0]) {
        setState(() {
          selected_laguage = 0;
        });
      } else {
        setState(() {
          selected_laguage = 1;
        });
        db.setDefaultLanguage(default_lang);
      }
    });
  }

  void setDefaultLanguage(String lang) async {
    await storage.write(key: "lang", value: lang);
  }

  @override
  Widget build(BuildContext context) {
    var _pageOptions = [
      Stack(
        children: <Widget>[
          selected_laguage == null ? Loading() : VideoSlidesShow(
            lang: selected_laguage,
          ),
          homeHeader( onLanguageChange),
        ],
      ),
      LikedPage(),
      // UserPage()
    ];


    var currentPage = MaterialApp(
      title: "You",
      theme: new ThemeData(primaryColor: Color.fromRGBO(58, 66, 86, 1.0)),
      home: Scaffold(
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: BottomNavyBar(
          backgroundColor: Colors.black,
          mainAxisAlignment: MainAxisAlignment.center,
          selectedIndex: _selectedPage,
          showElevation: false, // use this to remove appBar's elevation
          onItemSelected: (index) => setState(() {
            _selectedPage = index;
          }),
          items: [
            BottomNavyBarItem(
              icon: Icon(Icons.apps),
              title: Text('News'),
              activeColor: Colors.white,
            ),
            BottomNavyBarItem(
                icon: Icon(Icons.favorite_border),
                title: Text('Liked'),
                activeColor: Colors.grey),
          ],
        ),
      ),
    );

    return currentPage;
//    return PageView(
//      controller: ctrl,
//      scrollDirection: Axis.horizontal,
//      physics: physics,
//      pageSnapping: true,
//      children: <Widget>[
//        currentPage,
//        NewsWeb()
//      ],
//    );
  }
}

class MultiSelectChip extends StatefulWidget {
  final List<String> reportList;
  final Function(List<String>) onSelectionChanged;
  final String selectedItem;
  MultiSelectChip(this.reportList, this.selectedItem,
      {this.onSelectionChanged});

  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  List<String> selectedChoices = List();

  @override
  initState() {
    super.initState();
    setState(() {
      selectedChoices.add(widget.selectedItem);
    });
  }

  _buildChoiceList() {
    List<Widget> choices = List();

    widget.reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoices.contains(item),
          onSelected: (selected) {
            setState(() {
              if (!selectedChoices.contains(item)) {
                selectedChoices.clear();
                selectedChoices.add(item);
                widget.onSelectionChanged(selectedChoices);
              }
            });
          },
        ),
      ));
    });

    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
