import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';


class NewsSource extends StatefulWidget {
  @override
  _NewsSourceState createState() => new _NewsSourceState();
}

class _NewsSourceState extends State<NewsSource> {

  InAppWebViewController webView;
  String url = "";
  double progress = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      decoration:
      BoxDecoration(border: Border.all(color: Colors.blueAccent)),
      child: InAppWebView(
        initialUrl: "https://swipeupnews.com/",
        initialHeaders: {},
        initialOptions: InAppWebViewWidgetOptions(
            crossPlatform: InAppWebViewOptions(
              debuggingEnabled: false,
            )
        ),
        onWebViewCreated: (InAppWebViewController controller) {
          webView = controller;
        },
//        onLoadStart: (InAppWebViewController controller, String url) {
//          setState(() {
//            this.url = url;
//          });
//        },
//        onLoadStop: (InAppWebViewController controller, String url) async {
//          setState(() {
//            this.url = url;
//          });
//        },
//        onProgressChanged: (InAppWebViewController controller, int progress) {
//          setState(() {
//            this.progress = progress / 100;
//          });
//        },
      ),
    );
  }
}