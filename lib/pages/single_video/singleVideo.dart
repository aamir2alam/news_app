import 'package:flutter/material.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/widgets/home/controls/onscreen_controls.dart';
import 'package:news_app/widgets/home/header_back_button.dart';
import 'package:news_app/widgets/home/home_video_renderer.dart';

class SingleVideo extends StatelessWidget {
  final News news;
  SingleVideo({Key key, this.news}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView.builder(
      scrollDirection: Axis.vertical,
      itemCount: 1,
      itemBuilder: (context, int currentIndex) {
        return Container(
          color: Colors.black,
          child: Stack(
            children: <Widget>[
              AppVideoPlayer(news: news,isPause: false,),
            ],
          ),
        );
      },
    ));
  }
}
