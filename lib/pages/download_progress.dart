import 'package:flutter/material.dart';



  Widget DownloadProgress(progress) {
    return Opacity(
      opacity: 0.50,
      child:  Center(
        child: Container(
          alignment: Alignment.center,
          child: Card(
            elevation: 5,
            child: Container(
              height: 80,
              width: 80,
              alignment: Alignment.center,
              color: Colors.black,
              child: Text(progress, style: TextStyle(color: Colors.white,fontSize: 30.0),),
            ),
          ),
          constraints: BoxConstraints.expand(),
        ),
      )

    )

     ;
  }

