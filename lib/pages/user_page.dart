import 'package:flutter/material.dart';
import 'package:news_app/pages/videos_slide.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25.0),
      child: Text("Account", style: TextStyle(fontSize: 36.0)),
    );
  }
}
