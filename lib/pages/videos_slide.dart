import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:news_app/models/news_bloc/News.dart';
import 'package:news_app/pages/download_progress.dart';
import 'package:news_app/widgets/home/controls/onscreen_controls.dart';
import 'package:news_app/widgets/home/home_video_renderer.dart';
import 'package:provider/provider.dart';
import 'package:news_app/services/newsDbProvider.dart';
import 'package:news_app/services/authProvider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class VideoSlidesShow extends StatefulWidget {
  final int lang;
  VideoSlidesShow({Key key, this.lang}) : super(key: key);
  @override
  _VideoSlidesShowState createState() => _VideoSlidesShowState();
}

class _VideoSlidesShowState extends State<VideoSlidesShow> {
  final PageController ctrl = PageController();

  // keep track of current page to avoid unncessary renders
  int currentPage = 0;
  int currentFetchPage = 1;
  List slideList = [];
  String _download_progress = "0";
  bool _is_downloading = false;
  final storage = new FlutterSecureStorage();
  bool busy = false;
  CancelToken cancelToken = CancelToken();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    new Future.delayed(Duration.zero, () async {
      final db = Provider.of<NewsDbProvider>(context, listen: false);
      final auth = Provider.of<AuthServiceProvider>(context, listen: false);
      List items = await db.getNews(currentFetchPage, widget.lang);
      if (items.length == 0) {
        print("no news found");
      }
      print("language passed");
      print(widget.lang);
      currentFetchPage = currentFetchPage + 1;
      print(currentFetchPage);
      if (!mounted) {
        return;
      }
      setState(() {
        slideList.addAll(items);
      });
    });

    // Set state when page changes

    ctrl.addListener(() {
      int next = ctrl.page.round();
      if (currentPage != next) {
        setState(() {
          currentPage = next;
        });
      }
    });
  }

  @mustCallSuper
  @protected
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);

    new Future.delayed(Duration.zero, () async {
      final db = Provider.of<NewsDbProvider>(context, listen: false);
      setState(() {
        slideList = [];
        currentFetchPage = 1;
      });
      List items = await db.getNews(currentFetchPage, widget.lang);

      if (items.length == 2) {
        print("no news found");
      }
      print("total news items");
      print(items.length);
      print(currentFetchPage);
      if (!mounted) {
        return;
      }
      setState(() {
        slideList.addAll(items);
      });
    });

    // Set state when page changes
  }

  Future<void> share() async {
    String title = slideList[currentPage]["title"];
    String video_url = slideList[currentPage]["content"]["url"];
    FlutterShareMe().shareToWhatsApp(
        msg: '$title'
            ' : '
            ' $video_url '
            ' Swipe daily short news videos on the go. '
            ' www.swipeupnews.com ');
  }

  Future<bool> _onBackPressed() {
//    if(_is_downloading){
//      cancelToken.cancel();
//      setState(() {
//        _is_downloading = false;
//        _download_progress = "0";
//
//      });
//    }else{
//      exit(0);
//    }
//     showDialog(
//      context: context,
//      builder: (context) => new AlertDialog(
//        title: new Text('Are you sure?'),
//        content: new Text('Do you want to exit an App'),
//        actions:  <Widget>[
//          new GestureDetector(
//            onTap: () => Navigator.of(context).pop(false),
//            child: Text("NO"),
//          ),
//          SizedBox(height: 16),
//          new GestureDetector(
//            onTap: () => Navigator.of(context).pop(true),
//            child: Text("YES"),
//          ),
//        ],
//      ),
//    ) ;
  }

//  Future<void> share() async {
//    setState(() {
//      _is_downloading = true;
//    });
//    String file_path;
//    Dio dio = new Dio();
//
//    String download_url = slideList[currentPage]["content"]["url"];
//    print(download_url);
//    try {
//      var dir = await getApplicationDocumentsDirectory();
//      String d = DateTime.now().toIso8601String();
//      file_path = "${dir.path}/${d}swipeUpNews.mp4";
//      await dio.download(download_url, file_path, cancelToken: cancelToken,
//          onReceiveProgress: (rec, total) {
////          print("rec: $rec  total: $total ");
//        setState(() {
//          _download_progress = ((rec / total) * 100).toStringAsFixed(0) + "%";
//        });
//        print(_download_progress);
//      });
//    } catch (e) {
//      print(e);
//    }
//
//    if (_download_progress != "100%") {
//      return;
//    }
//    // share downloaded file
//    if (!_is_downloading) {
//      return;
//    }
//    print(file_path);
//    ShareExtend.share(file_path, "file");
//
//    setState(() {
//      _is_downloading = false;
//      _download_progress = "0";
//    });
//  }

  void _handlePageChanged(int page) {
    print(page);

    if (slideList.length - 2 == page && currentFetchPage != -1) {
      new Future.delayed(Duration.zero, () async {
        final db = Provider.of<NewsDbProvider>(context, listen: false);
        List items = await db.getNews(currentFetchPage, widget.lang);
        print(items.length);
        print("news items");
        if (items.length == 2) ;
//        print(slideList.length);
        currentFetchPage = currentFetchPage + 1;
        if (items.length < 10) currentFetchPage = -1;
        print(currentFetchPage);
        setState(() {
          slideList.addAll(items);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<NewsDbProvider>(context);
//    slides2 = db.getNewsSlides();
    // stream build react in realtime when any of the backend data changes
    if (slideList.length == 0) {
      return Stack(
        children: <Widget>[
          Scaffold(
            backgroundColor: Color.fromRGBO(0, 0, 0, 1.0),
            body: Align(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            ),
          ),
        ],
      );
    } else if (slideList.length > 0) {
      return IgnorePointer(
        ignoring: _is_downloading,
        child: PageView.builder(
          controller: ctrl,
          scrollDirection: Axis.vertical,
          onPageChanged: _handlePageChanged,
          itemCount: slideList.length,
          itemBuilder: (context, int currentIndex) {
            // to check which page is active
            bool active = currentIndex == currentPage;

            // data to pass : slideList[currentIndex - 1]
            var data = slideList[currentIndex];
            String _id = data["_id"];
            String url = data["content"]["url"];
            String web_url = data["url"];
            String title = data["title"];
            String desc = data["description"];
            int likes = data["likes_count"];
            bool isLiked = data["is_liked"];
            String img = data["thumbnail"];
            News news = News(_id, url,web_url, title, desc, likes, isLiked, img);
            return Container(
              color: Colors.black,
              child: Stack(
                children: <Widget>[
                  AppVideoPlayer(news: news, isPause: _is_downloading),
                  _is_downloading
                      ? DownloadProgress(_download_progress)
                      : Text(""),
                  onScreenControls(
                      news, db.likeNewsbyId, share, false, _download_progress)
                ],
              ),
            );
          },
        ),
      );
    } else {
      <Widget>[
        SizedBox(
          child: CircularProgressIndicator(),
          width: 60,
          height: 60,
        ),
      ];
    }

//    return StreamBuilder(
//      stream: slides2,
//      initialData: [],
//      builder: (context, AsyncSnapshot snap) {
//        List slideList = snap.data.toList();
//        return PageView.builder(
//          controller: ctrl,
//          scrollDirection: Axis.vertical,
//          itemCount: slideList.length,
//          itemBuilder: (context, int currentIndex) {
//            // to check which page is active
//            bool active = currentIndex == currentPage;
//
//            // data to pass : slideList[currentIndex - 1]
//            var data = slideList[currentIndex];
//            String _id = data["_id"];
//            String url = data["content"]["url"];
//            String title = data["title"];
//            String desc = data["description"];
//            int likes = data["likes"];
//            News news = News(_id, url, title, desc, likes);
//            return Container(
//              color: Colors.black,
//              child: Stack(
//                children: <Widget>[
//                  AppVideoPlayer(news: news),
//                  onScreenControls(news, db)
//                ],
//              ),
//            );
//          },
//        );
//      },
//    );
  }
}
