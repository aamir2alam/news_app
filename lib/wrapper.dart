import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:news_app/pages/bottom_navigation.dart';
import 'package:news_app/pages/loading.dart';
import 'package:news_app/services/newsDbProvider.dart';
import 'package:provider/provider.dart';
import 'package:news_app/services/authProvider.dart';

class Wrapper extends StatefulWidget{

  Wrapper();
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {

  String _uid;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fcm.subscribeToTopic("newsNotification");
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        // TODO  handle notification when the app is in foreground
//        showDialog(
//          context: context,
//          builder: (context) => AlertDialog(
//            content: ListTile(
//              title: Text(message['notification']['title']),
//              subtitle: Text(message['notification']['body']),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                child: Text('Ok'),
//                onPressed: () => Navigator.of(context).pop(),
//              ),
//            ],
//          ),
//        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional  when app is closed on click on notifications

      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional  when app is minimised on click on notifications

      },
    );

    new Future.delayed(Duration.zero, () async {

      final auth = Provider.of<AuthServiceProvider>(context, listen: false);
      print("signing started");
      if(auth.getUid() == null){
        String uid = await auth.signInAnon();

        setState(() {
          _uid = uid;
        });
        print("uid after signin "+_uid);
      }else{

        setState(() {
          _uid = auth.getUid();
        });
        print("uid  "+_uid);
      }

    });

  }

  @override
  Widget build(BuildContext context) {


    final bottom_navigation = ChangeNotifierProvider<NewsDbProvider>(
      create: (context) => NewsDbProvider(),
      child: BottomNavigationPage(),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: _uid != null ? bottom_navigation : Loading(),
      ),
    );
  }
}
