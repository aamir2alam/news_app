import 'package:flutter/material.dart';
import 'package:news_app/services/authProvider.dart';
import 'package:news_app/wrapper.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: Scaffold(
      //   body: BottomNavigationPage(),
      //   // body: Stack(
      //   //   children: <Widget>[
      //   //     VideoSlidesShow(),
      //   //     BottomNavigation(),
      //   //     homeHeader(),
      //   //   ],
      //   // ),
      // ),
      home: ChangeNotifierProvider<AuthServiceProvider>(
        create: (context) => AuthServiceProvider(null),
        child: Wrapper(),
      ),
    );
  }
}
// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(body: FirestoreSlideShow()),
//     );
//   }
// }

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(debugShowCheckedModeBanner: false, home: TestPage());
//   }
// }
